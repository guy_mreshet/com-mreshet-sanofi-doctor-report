﻿package  
{
	public class PatientDetails 
	{
		
		 public var pName:String = "";
		 public var pWeight:int = -1;
		 public var pHight:int = -1;
		 public var pTargetSugarFast:int = -1;
		 public var pTargetSugarMeal:int = -1;
		 public var pDrug1:String = "";
         public var pDrug2:String = "";
         public var pDrug3:String = "";
		 public var pDrug4:String = "";
		 
		 public function PatientDetails()
		 {
			 
		 }

		 public function getDrugsNum():int
		 {
			 var cntr:int = 0;
			 cntr += pDrug1 == "" ? 0 : 1;
			 cntr += pDrug2 == "" ? 0 : 1;
			 cntr += pDrug3 == "" ? 0 : 1;
			 cntr += pDrug4 == "" ? 0 : 1;
			 return cntr;
		 }
		 public function drugExists(drg:String):Boolean
	 	 {
			if (drg == pDrug1 || drg == pDrug2 || drg == pDrug3 || drg == pDrug4 || drg == "")
				return true;
			return false;
		 }
    }
}
