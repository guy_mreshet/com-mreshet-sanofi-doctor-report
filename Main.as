﻿package  {
	
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.events.Event;
	import com.adobe.images.JPGEncoder;
	import com.adobe.images.PNGEncoder;
	import flash.utils.ByteArray;
	import flash.net.FileReference;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
 	import flash.utils.getQualifiedClassName;
 	import flash.geom.Matrix;
	
	public class Main extends Sprite 
	{
		private var _reportMngr:ReportMngr = new ReportMngr();
		
		/** 
		* Test function.
		* 
		* The class ReportMngr must has 8 images files: "1.png", "1-2.png", "2.png", "2-2.png", "3.png", "3-2.png", "4.png", "4-2.png"
		* These images are the templated for the reports
		* 
		* @return void
		*/ 
		public function Main():void
		{
			
			//reportMngr.addPersonalDetails("Guy", 74, 44, 65, 75, "Drug1");  // works!
			//reportMngr.addPersonalDetails("Guy", 74, 44, 65, 75, "Drug1", "Drug2"); //works!
			//reportMngr.addPersonalDetails("Guy", 74, 44, 65, 75, "Drug1", "Drug2","Drug3");
			_reportMngr.addPersonalDetails("Guy", 74, 44, 65, 75, "Drug1", "Drug2","Drug3","Drug4"); //works
			_reportMngr.addHemoglobinDetails("18-9-2044","18:00", 5);
			
			for (var i:int = 0; i < 50; i++)
				_reportMngr.addIndex("13-4-2014", "18:00" ,Math.random()*100, Math.random()*100, Math.random()*100, Math.random()*100, Math.random()*100, Math.random()*100, "אלו ההערות שלי");
			
			_reportMngr.addEventListener(Event.COMPLETE, onComplete);
			_reportMngr.drawTables();
			addChild(_reportMngr);
		}
		
		/** 
		* Output completed Function, images are ready. output images are at _reportMngr.getRes();
		* 
		* @param e - Event. 
		* 
		* @return void
		*/ 
		private function onComplete(e:Event):void
		{
			var arr:Array = _reportMngr.getRes();
			var sprite:Sprite = arr[0]; //choose number of page you want to view
			//var bytes:ByteArray = PNGEncoder.encode(getMovieClipAsBitmap(spriteBG));
			//var file:FileReference    = new FileReference();
			//file.save( bytes, "Image.png" );
			sprite.scaleX = 0.35;
			sprite.scaleY = 0.35;
			addChild(sprite);
		}
		
		/** 
		* Utility function. converts Sprite to BMP.
		* 
		* @param sprite - Input Sprite to be converted into Bitmap
		* 
		* @return Sprite's BitmapData
		*/ 
		private function getMovieClipAsBitmap(sprite:Sprite):BitmapData
		{
			var bounds:Rectangle = sprite.getBounds(sprite);
			var theBitmap:Bitmap = new Bitmap(new BitmapData(bounds.width, bounds.height, true, 0x0));
			var m:Matrix = new Matrix(1, 0, 0, 1, -bounds.x, -bounds.y);
			theBitmap.bitmapData.draw(sprite, m, null, null, null, true);
			return theBitmap.bitmapData;
		}
		
	}
}
