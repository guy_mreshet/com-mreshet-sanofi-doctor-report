﻿package  
{
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.geom.Point
	import flash.text.TextFormatAlign;
	import flash.events.Event;
	public class ReportMngr extends Sprite 
	{
		private var _patientDetails:PatientDetails = new PatientDetails();				//Patient's general details
		private var _hemoglobinDetails:HemoglobinDetails = new HemoglobinDetails();		//Patient's Hemoglobin details
		private var _indexesArr:Array = new Array();									//Patient's indexed data
		private var _spritesArr:Array = new Array();									// Res array
		private var _drugsNum:int = 0;													// Number of drugs beeing used can be between 1-4
		private var _currentPage:int = 0;												// Utility variable, current page beeing proccessed
		private var _cntrImgs:int = 0;													// Utility variable, how many bg has been loaded so far
		private var _numExtraTables:int = 0;											// Number of indexes images
		private var NUM_FIELDS_FIRST_PAGE = 12;											// Number of rows at first page
		private var NUM_FIELDS_PAGE = 21;												// Number of rows at the following pages
		
		/** 
		* Empty Constructor
		*/ 
		public function ReportMngr()
		{
		}
		
		/** 
		* Public function for adding patient's details
		* Should be called one time
		*/ 
		public function addPersonalDetails(patientName:String, 
										   patientWeight:int = -1, 
										   patientHight:int = -1, 
										   patientTargetSugarFast: int = -1, 
										   patientTargetSugarMeal: int = -1, 
										   drugName1:String = "",
										   drugName2:String = "",
										   drugName3:String = "",
										   drugName4:String = ""):void
		{
			_patientDetails.pName = patientName;
			_patientDetails.pWeight = patientWeight;
			_patientDetails.pHight = patientHight;
			_patientDetails.pTargetSugarFast = patientTargetSugarFast;
			_patientDetails.pTargetSugarMeal = patientTargetSugarMeal;
			if (drugName1 != "")
				_drugsNum++;
			if (drugName2 != "")
				_drugsNum++;
			if (drugName3 != "")
				_drugsNum++;
			if (drugName4 != "")
				_drugsNum++;
				
			_patientDetails.pDrug1 = drugName1;
			_patientDetails.pDrug2 = drugName2;
			_patientDetails.pDrug3 = drugName3;
			_patientDetails.pDrug4 = drugName4;
		}
		
		/** 
		* Public function for adding patient's hemoglobin details
		* Should be called one time
		*/ 
		public function addHemoglobinDetails(date:String = "",
											 hour:String = "",
											 lvl:int = -1):void
		{
			_hemoglobinDetails.date = date;
			_hemoglobinDetails.hour = hour;
			_hemoglobinDetails.lvl = lvl == -1 ? "" : lvl.toString();
		}
		
		/** 
		* Public function for adding patient's index data
		* Should be called as many times as needed
		*/ 
		public function addIndex(date:String,
								 hour:String,
								 sugarFast:int = -1,
								 sugarMeal:int = -1,
								 drug1:int = -1,
								 drug2:int = -1,
								 drug3:int = -1,
								 drug4:int = -1,
								 remarks:String = ""):Boolean
		{
			var index:IndexDetails = new IndexDetails();
			index.date = date;
			index.hour = hour;
			index.sugarFast = sugarFast;
			index.sugarMeal = sugarMeal;
			index.drug1 = drug1;
			index.drug2 = drug2;
			index.drug3 = drug3;
			index.drug4 = drug4;
			index.remarks = remarks;
			
			_indexesArr.push(index);
			return true;
		}
		
		/** 
		* Public function in order to start rendering after add data has been added.
		*/ 
		public function drawTables():void
		{
			if (_drugsNum == 0) //use the table with one drug, and leave it empty
				_drugsNum++;
			var i:int = 0;
			_spritesArr.push(new Sprite()); // First page
			if (_indexesArr.length > NUM_FIELDS_FIRST_PAGE)
			{
				_numExtraTables = Math.ceil((_indexesArr.length - NUM_FIELDS_FIRST_PAGE)/NUM_FIELDS_PAGE);
				trace("_numExtraTables = " + _numExtraTables);
				for (i = 0; i < _numExtraTables; i++)
				{
					_spritesArr.push(new Sprite());
				}
			}
			
			var myLoader1:Loader = new Loader();
			myLoader1.contentLoaderInfo.addEventListener(Event.COMPLETE, onImgLoaded1);
			myLoader1.load(new URLRequest(_drugsNum.toString()+".png"));
			
			for (i = 0; i < _numExtraTables; i++)
			{
				var myLoader2:Loader = new Loader();
				myLoader2.contentLoaderInfo.addEventListener(Event.COMPLETE, onImgLoaded2);
				myLoader2.load(new URLRequest(_drugsNum.toString()+"-2.png"));
			}
		}
		
		/** 
		* Public function for getting the result
		* Should be called after Event.Complete was fired
		*/ 
		public function getRes():Array
		{
			return _spritesArr;
		}
		
		/** 
		* Private function for drawing all data
		*/ 
		private function startDrawing():void
		{
			var sprite:Sprite = _spritesArr[0];
			
			drawPersonalDetails();
			drawHemoglobinDetailsDetails();
			drawIndexes();
			
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		/** 
		* Private function for loading the background for the first image
		*/ 
		private function onImgLoaded1(e:Event):void
		{
			_spritesArr[0].addChild(e.target.content);
			_cntrImgs++;
			if(_cntrImgs == _numExtraTables+1)
				startDrawing();
		}

		/** 
		* Private function for loading the background for the following pages (all except the first page)
		*/ 
		private function onImgLoaded2(e:Event):void
		{
			trace("onImgLoaded2");
			_cntrImgs++;
			_spritesArr[_cntrImgs].addChild(e.target.content);
			
			
			if(_cntrImgs == _numExtraTables+1)
				startDrawing();
		}
		
		/** 
		* Private function for rendering paitent's details
		*/ 
		private function drawPersonalDetails():void
		{
			var sprite:Sprite = _spritesArr[0];
			var col:int = 0;
			
			sprite.addChild(addCell("PatientDetails", _patientDetails.pName, 0, col++));
			sprite.addChild(addCell("PatientDetails", _patientDetails.pWeight == -1 ? "" : _patientDetails.pWeight.toString(), 0, col++));
			sprite.addChild(addCell("PatientDetails", _patientDetails.pHight == -1 ? "" :  _patientDetails.pHight.toString(), 0, col++));
			sprite.addChild(addCell("PatientDetails", _patientDetails.pTargetSugarFast == -1 ? "" : _patientDetails.pTargetSugarFast.toString(), 0, col++));
			sprite.addChild(addCell("PatientDetails", _patientDetails.pTargetSugarMeal == -1 ? "" : _patientDetails.pTargetSugarMeal.toString(), 0, col++));
			var drugs:String = "";
			drugs += _patientDetails.pDrug1;
			drugs += _patientDetails.pDrug2 == "" ? "" : ", " + _patientDetails.pDrug2;
			drugs += _patientDetails.pDrug3 == "" ? "" : ", " + _patientDetails.pDrug3;
			drugs += _patientDetails.pDrug4 == "" ? "" : ", " + _patientDetails.pDrug4;
			sprite.addChild(addCell("PatientDetails", drugs , 0, col++));
		}
		
		/** 
		* Private function for rendering paitent's hemoglobin details
		*/
		private function drawHemoglobinDetailsDetails():void
		{
			var sprite:Sprite = _spritesArr[0];
			var col:int = 0;
			
			sprite.addChild(addCell("Hemoglobin", _hemoglobinDetails.date, 0, col++));
			sprite.addChild(addCell("Hemoglobin", _hemoglobinDetails.hour, 0, col++));
			sprite.addChild(addCell("Hemoglobin", _hemoglobinDetails.lvl, 0, col++));
		}
		
		/** 
		* Private function for rendering paitent's index details - all rows that have been added
		*/
		private function drawIndexes():void
		{
			var sprite:Sprite = _spritesArr[_currentPage];
			addDrugsTitles();
			for (var i:int=0; i<_indexesArr.length; i++)
			{
				if ((i == NUM_FIELDS_FIRST_PAGE) ||  												//finished the first page
				   ((i-NUM_FIELDS_FIRST_PAGE)%NUM_FIELDS_PAGE == 0 && i > NUM_FIELDS_FIRST_PAGE))	//modulo number of pages, except the first one
				{
					//_spritesArr.push(new Sprite());
					_currentPage++;
					sprite = _spritesArr[_currentPage];
					addDrugsTitles();
				}
				var col:int = 0;
				var j:int = getNewInd(i);
				//trace("i = " + i + ", j = " + j);
				var indexDetails:IndexDetails = _indexesArr[i];
				sprite.addChild(addCell("Index", indexDetails.date, j, col++));
				sprite.addChild(addCell("Index", indexDetails.hour, j, col++));
				sprite.addChild(addCell("Index", indexDetails.sugarFast == -1 ? "" : indexDetails.sugarFast.toString(), j, col++));
				sprite.addChild(addCell("Index", indexDetails.sugarMeal == -1 ? "" : indexDetails.sugarMeal.toString(), j, col++));
				sprite.addChild(addCell("Index", indexDetails.drug1 == -1 ? "" : indexDetails.drug1.toString(), j, col++));
				if (_drugsNum > 1) {sprite.addChild(addCell("Index", indexDetails.drug2 == -1 ? "" : indexDetails.drug2.toString(), j, col++));}
				if (_drugsNum > 2) {sprite.addChild(addCell("Index", indexDetails.drug3 == -1 ? "" : indexDetails.drug3.toString(), j, col++));}
				if (_drugsNum > 3) {sprite.addChild(addCell("Index", indexDetails.drug4 == -1 ? "" : indexDetails.drug4.toString(), j, col++));}
				sprite.addChild(addCell("Index", indexDetails.remarks, j, col++));
			}
		}
		
		/** 
		* Private utility function for computing local image row index
		*/
		private function getNewInd(i:int):int
		{
			if (i<NUM_FIELDS_FIRST_PAGE)
				return i;
			else 
				return ((i-NUM_FIELDS_FIRST_PAGE) - (NUM_FIELDS_PAGE*(_currentPage-1)));
		}
		
		/** 
		* Private utility function for adding titles for the current page
		*/
		private function addDrugsTitles():void
		{
			var sprite:Sprite = _spritesArr[_currentPage];
			sprite.addChild(addCell("DrugTitle1", _patientDetails.pDrug1, 1, _drugsNum));
			sprite.addChild(addCell("DrugTitle2", _patientDetails.pDrug2, 2, _drugsNum));
			sprite.addChild(addCell("DrugTitle3", _patientDetails.pDrug3, 3, _drugsNum));
			sprite.addChild(addCell("DrugTitle4", _patientDetails.pDrug4, 4, _drugsNum));
		}
		
		/** 
		* Private utility function for adding elemnt by position
		*/
		private function addCell(type:String, val:String, row:int, col:int):TextField
		{
			//trace("type = " + type + " val = " + val);
			var txt:TextField = new TextField();
			txt.text = val;
			if (0)
			{
				txt.background = true;
				var rc:RandomColor = new RandomColor();
				txt.backgroundColor = rc.getRandomColor();
			}
			var format:TextFormat = new TextFormat();
			format.size = 60;
			format.align = TextFormatAlign.CENTER;
			txt.setTextFormat(format);
			txt.height = 75;
			if (val == "")
			{
				txt.width = 0;
				return txt;
			}
			if (type == "PatientDetails")
			{
				txt.y = 600;
				if (col == 0) {txt.x = 2999; txt.width = 355;}
				else if (col == 1) { txt.x = 2745; txt.width = 250;}
				else if (col == 2) { txt.x = 2482; txt.width = 255;}
				else if (col == 3) { txt.x = 2100; txt.width = 230;}
				else if (col == 4) { txt.x = 1580; txt.width = 240;}
				else if (col == 5) { txt.x = 145;  txt.width = 1432;}
			}
			else if (type == "Hemoglobin")
			{
				txt.y = 916;
				if (col == 0) {txt.x = 2999; txt.width = 355;}
				else if (col == 1) { txt.x = 2745; txt.width = 250;}
				else if (col == 2) { txt.x = 2242; txt.width = 500;}
			}
			else if (type == "Index")
			{
				if (_currentPage == 0)
				{
					txt.y = 1225 + row*71;
					if (col == 0) {txt.x = 2999; txt.width = 355;}			//date
					else if (col == 1) {txt.x = 2745; txt.width = 250;}		//hour
					else if (col == 2) {txt.x = 2429; txt.width = 312;}		//fast
					else if (col == 3) {txt.x = 1966; txt.width = 463;}		//meal
					if (_drugsNum == 1)
					{
						if (col == 4) {txt.x = 1434; txt.width = 532;}   	//Drug1
						if (col == 5) {txt.x = 145;  txt.width = 1286;}		//Remarks
					}
					else if (_drugsNum == 2)
					{
						if (col == 4) {txt.x = 1525; txt.width = 440;}		  //Drug1
						else if (col == 5) {txt.x = 1076;  txt.width = 440;}  //Drug2
						else if (col == 6) {txt.x = 150;  txt.width = 929;}   //Remarks
					}
					else if (_drugsNum == 3)
					{
						if (col == 4) {txt.x = 1590; txt.width = 375;}		  //Drug1
						else if (col == 5) {txt.x = 1217;  txt.width = 375;}  //Drug2
						else if (col == 6) {txt.x = 842;  txt.width = 375;}   //Drug3
						else if (col == 7) {txt.x = 150;  txt.width = 690;}   //Remarks
					}
					else if (_drugsNum == 4)
					{
						if (col == 4) {txt.x = 1646; txt.width = 323;}		  //Drug1
						else if (col == 5) {txt.x = 1321;  txt.width = 323;}  //Drug2
						else if (col == 6) {txt.x = 1000;  txt.width = 323;}   //Drug3
						else if (col == 7) {txt.x = 678;  txt.width = 323;}   //Drug4
						else if (col == 8) {txt.x = 150;  txt.width = 529;}   //Remarks
					}
				}
				else if (_currentPage > 0)
				{
					txt.y = 606 + row*71;
					if (col == 0) {txt.x = 2999; txt.width = 355;}			//date
					else if (col == 1) {txt.x = 2745; txt.width = 250;}		//hour
					else if (col == 2) {txt.x = 2429; txt.width = 312;}		//fast
					else if (col == 3) {txt.x = 1966; txt.width = 463;}		//meal
					if (_drugsNum == 1)
					{
						if (col == 4) {txt.x = 1434; txt.width = 535;}	//drug1
						else if (col == 5) {txt.x = 145;  txt.width = 1282;} //remarks
					}
					else if (_drugsNum == 2)
					{
						if (col == 4) {txt.x = 1522; txt.width = 440;}		 //drug1
						else if (col == 5) {txt.x = 1076;  txt.width = 440;} //drug2
						else if (col == 6) {txt.x = 147;  txt.width = 930;}  //remarks
					}
					else if (_drugsNum == 3)
					{
						if (col == 4) {txt.x = 1591; txt.width = 374;}		 //drug1
						else if (col == 5) {txt.x = 1216;  txt.width = 374;} //drug2
						else if (col == 6) {txt.x = 841;  txt.width = 374;}  //drug3
						else if (col == 7) {txt.x = 147;  txt.width = 700;}  //remarks
					}
					else if (_drugsNum == 4)
					{
						if (col == 4) {txt.x = 1644; txt.width = 322;}		 //drug1
						else if (col == 5) {txt.x = 1322;  txt.width = 322;} //drug2
						else if (col == 6) {txt.x = 996;  txt.width = 322;}  //drug3
						else if (col == 7) {txt.x = 677;  txt.width = 322;}  //drug4
						else if (col == 8) {txt.x = 147;  txt.width = 527;}  //remarks
					}
				}
			}
			else if (type == "DrugTitle1")
			{
				if (_drugsNum == 1 && _currentPage == 0) {txt.x = 1431; txt.y = 1145; txt.width = 535;} //done
				if (_drugsNum == 1 && _currentPage > 0) {txt.x = 1431; txt.y = 527; txt.width = 535;}   //done
				
				if (_drugsNum == 2 && _currentPage == 0) {txt.x = 1525; txt.y = 1145; txt.width = 442;}  //done
				if (_drugsNum == 2 && _currentPage > 0) {txt.x = 1520; txt.y = 527; txt.width = 448;}    //done
				
				if (_drugsNum == 3 && _currentPage == 0) {txt.x = 1594; txt.y = 1145; txt.width = 372;}  
				if (_drugsNum == 3 && _currentPage > 0) {txt.x = 1594; txt.y = 527; txt.width = 372;}  
				
				if (_drugsNum == 4 && _currentPage == 0) {txt.x = 1643; txt.y = 1145; txt.width = 323;}   //done
				if (_drugsNum == 4 && _currentPage > 0) {txt.x = 1644; txt.y = 527; txt.width = 323;}   //done
			}
			else if (type == "DrugTitle2")
			{
				trace("_currentPage = " + _currentPage);
				if (_drugsNum == 2 && _currentPage == 0) {txt.x = 1080; txt.y = 1145; txt.width = 442;}   //done
				if (_drugsNum == 2 && _currentPage > 0) {txt.x = 1076; txt.y = 527; txt.width = 448;}    //done
				
				if (_drugsNum == 3 && _currentPage == 0) {txt.x = 1217; txt.y = 1145; txt.width = 372;}   //done
				if (_drugsNum == 3 && _currentPage > 0) {txt.x = 1218; txt.y = 527; txt.width = 372;}    //done
				
				if (_drugsNum == 4 && _currentPage == 0) {txt.x = 1321; txt.y = 1145; txt.width = 323;}   //done
				if (_drugsNum == 4 && _currentPage > 0) {txt.x = 1321; txt.y = 527; txt.width = 323;}   //done
			}
			else if (type == "DrugTitle3")
			{
				if (_drugsNum == 3 && _currentPage == 0) {txt.x = 842; txt.y = 1145; txt.width = 372;}   //done
				if (_drugsNum == 3 && _currentPage > 0) {txt.x = 842; txt.y = 527; txt.width = 372;}  
				
				if (_drugsNum == 4 && _currentPage == 0) {txt.x = 1000; txt.y = 1145; txt.width = 323;}   //done
				if (_drugsNum == 4 && _currentPage > 0) {txt.x = 1000; txt.y = 527; txt.width = 372;}  
			}
			else if (type == "DrugTitle4")
			{
				if (_drugsNum == 4 && _currentPage == 0) {txt.x = 678; txt.y = 1145; txt.width = 323;}   //done
				if (_drugsNum == 4 && _currentPage > 0) {txt.x = 678; txt.y = 527; txt.width = 323;}  
			}
			return txt;
		}
	}
}
